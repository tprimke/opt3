module.exports = {
  'Success test' : function (browser) {
    browser
      .url('http://localhost:1337/index.html')
      .waitForElementVisible('body', 3000)
      .pause(1000)
      .assert.containsText('div#app > p', 'Success!')
      .pause(1000)
      .end();
  }
};
