var should = require('should'),
    Sim    = require('../../src/js/sim1');

describe('Sim1', () => {

  it("Initial, empty state", () => {
    let state = Sim.getState();

    state.should.deepEqual({});
  });

  let simpleActionTest = (spec) =>
    it(spec.title, () => {
      // run all the actions
      spec.actions.forEach( (action) => {
        if ( isNaN(action) )
          // common action
          Sim.action(action)
        else {
          // just tick the clock
          for (let i = 0; i < action; i++ )
            Sim.action({ type: 'tick' });
        }
      });

      // calculate the states
      let expected_state = Object.assign({}, spec.base_state, spec.extra_state),
          actual_state   = Sim.getState();

      // check the states
      actual_state.should.deepEqual(expected_state);
    });

  describe('The simplest system', () => {

    const SPEC = {
      // only cash
      enterprise_resources: [
        { id: 'cash', value: 20, label: 'B' }
      ],

      // 1 raw -> 1 product
      products: [
        { id: 'raw', name: 'Raw material' },
        { id: 'product', name: 'Product' }
      ],

      // at least one resource is needed...
      resources: [
        { id: 'res', colour: '#0000ff', number: 1 }
      ],

      // only one operation...
      operations: [
        { id: 'op',
          spec: [
            { id: 'raw', number: 1, type: 'in' },
            { id: 'product', number: 1, type: 'out' }
          ] }
      ],

      // ...and one machine
      machines: [
        { id: 'machine',
          operations: [
            { id: 'op', time: 10 }
          ],
          resources: [
            { id: 'res', required: 1 }
          ],
          setup: 12 }
      ],

      // and, finally, one station
      stations: [
        { id: 'station',
          machine: 'machine',
          name: 'Station',
          connections: [
            { type: 'sink', id: 'market' }
          ] }
      ],

      // one source
      sources: [
        { id: 'raw',
          name: 'Raw material',
          product: 'raw',
          price: [
            { id: 'cash', value: 10 }
          ],
          connections: [ 'station' ] }
      ],

      // and one sink
      sinks: [
        { id: 'market',
          name: 'Market',
          product: 'product',
          price: [
            { id: 'cash', value: 30 }
          ] }
      ]

    };

    const INITIAL_STATE = {
      system: [
        { label: 'B', value: 20 }
      ],

      time: 0,

      sources: [
        { id: 'raw', name: 'Raw material', price: 10, label: 'B', bought: 0 }
      ],

      sinks: [
        { id: 'market', name: 'Market', price: 30, label: 'B', sold: 0 }
      ],

      stations: [
        { id: 'station',
          name: 'Station',
          state: 'not-ready',
          buffer: [
            { id: 'raw', qty: 0 },
            { id: 'product', qty: 0 }
          ] }
      ]
    };

    beforeEach( () => {
      Sim.loadSpec(SPEC);
    });

    simpleActionTest({
      title:       "Load the spec",
      base_state:  INITIAL_STATE,
      actions:     [],
      extra_state: {}
    });

    simpleActionTest({
      title:       "Buy the raw material",
      base_state:  INITIAL_STATE,
      actions:     [
        { type: 'buy', raw: 'raw', qty: 1 }
      ],
      extra_state: {
        system: [
          { label: 'B', value: 10 }
        ],

        sources: [
          { id: 'raw', name: 'Raw material', price: 10, label: 'B', bought: 1 }
        ],

        stations: [
          { id: 'station',
            name: 'Station',
            state: 'not-ready',
            buffer: [
              { id: 'raw', qty: 1 },
              { id: 'product', qty: 0 }
            ] }
        ]
      }
    });

    simpleActionTest({
      title:       "Start the setup",
      base_state:  INITIAL_STATE,
      actions:     [
        { type: 'setup', station: 'station' }
      ],
      extra_state: {
        stations: [
          { id: 'station',
            name: 'Station',
            state: 'setup',
            time: 12,
            buffer: [
              { id: 'raw', qty: 0 },
              { id: 'product', qty: 0 }
            ] }
        ]
      }
    });

    simpleActionTest({
      title:       "Setup the station",
      base_state:  INITIAL_STATE,
      actions:     [
        { type: 'setup', station: 'station' },
        12
      ],
      extra_state: {
        time: 12,
        
        stations: [
          { id: 'station',
            name: 'Station',
            state: 'idle',
            buffer: [
              { id: 'raw', qty: 0 },
              { id: 'product', qty: 0 }
            ] }
        ]
      }
    });
    
    simpleActionTest({
      title:       "Almost produce the product",
      base_state:  INITIAL_STATE,
      actions:     [
        { type: 'buy', raw: 'raw', qty: 1 },
        { type: 'setup', station: 'station' },
        12 + 9
      ],
      extra_state: {
        system: [
          { label: 'B', value: 10 }
        ],

        time: 12 + 9,
        
        sources: [
          { id: 'raw', name: 'Raw material', price: 10, label: 'B', bought: 1 }
        ],

        stations: [
          { id: 'station',
            name: 'Station',
            state: 'busy',
            time: 1,
            buffer: [
              { id: 'raw', qty: 0 },
              { id: 'product', qty: 0 }
            ] }
        ]
      }
    });
    
    // Since there is only one sink, all the produced products
    // should be immediatelly sold.
    simpleActionTest({
      title:       "Produce and sell the product",
      base_state:  INITIAL_STATE,
      actions:     [
        { type: 'buy', raw: 'raw', qty: 1 },
        { type: 'setup', station: 'station' },
        12 + 10
      ],
      extra_state: {
        system: [
          { label: 'B', value: 40 }
        ],

        time: 12 + 10,
        
        sources: [
          { id: 'raw', name: 'Raw material', price: 10, label: 'B', bought: 1 }
        ],

        sinks: [
          { id: 'market', name: 'Market', price: 30, label: 'B', sold: 1 }
        ],

        stations: [
          { id: 'station',
            name: 'Station',
            state: 'idle',
            time: 1,
            buffer: [
              { id: 'raw', qty: 0 },
              { id: 'product', qty: 0 }
            ] }
        ]
      }
    });
  });

});

