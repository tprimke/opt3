var Redux = require('redux');

var handlers = {};

// ------------------------------------------------------------------
// API
// ------------------------------------------------------------------

var getState = function() {
  return model.getState();
};

exports.getState = getState;

var action = function( action ) {
  model.dispatch( action );
};

exports.action = action;

var loadSpec = function( spec ) {
  action( { type: 'spec', data: spec } );
};

exports.loadSpec = loadSpec;


// ------------------------------------------------------------------
// Actions and reducers
// ------------------------------------------------------------------

const INITIAL_STATE = {};

var sim1App = function( state = INITIAL_STATE, action ) {
  let type = action.type;
  if ( handlers.hasOwnProperty(type) ) {
    let handler = handlers[type],
        state_update = handler(state, action);

    return Object.assign({}, state, state_update);
  }

  return state;
};

var model = Redux.createStore(sim1App);

var defineAction = function( name, handler ) {
  handlers[name] = handler;
};

// Loads the specification, and initiates the state.
// The old state is ignored.
defineAction('spec', (_state, action) => {
  let spec = action.data;

  // system
  let system = spec.enterprise_resources.map( (res) => {
    return {
      label: res.label,
      value: res.value
    };
  });

  // TODO:
  // time
  // sources
  // sinks
  // stations

  return {
    system
  };
});

// Ticks the clock.
defineAction('tick', (state, _action) => {
  return {};
});

// Buys some raw material.
defineAction('buy', (state, action) => {
  return {};
});

// Starts a setup of a station.
// (May be impossible to perform, when there are not enough of available
// resources.)
defineAction('setup', (state, action) => {
  return {};
});
