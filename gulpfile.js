var gulp       = require('gulp'),
    babel      = require('gulp-babel'),
    del        = require('del'),
    rename     = require('gulp-rename'),
    mocha      = require('gulp-mocha'),
    webpack    = require('webpack-stream'),
    uglify     = require('gulp-uglify'),
    pump       = require('pump'),
    server     = require('./test/server');

// ------------------------------------------------------------------
// Settings

  // the name of the single, target JS file
var APPLICATION = 'app.js';

  // for the e2e tests, the root directory for the HTTP server
var HTTP_ROOT   = './dist';
  // ...and the port number
var HTTP_PORT   = 1337;


// ------------------------------------------------------------------
// Auxiliary functions

var renameJS = (path) => path.extname = '.js';

var compile = (pth, dest) => {
  var presets = [ 'es2015', 'react' ];

  return gulp.src(pth)
             .pipe( babel({ presets: presets }) )
             .pipe( rename( renameJS ) )
             .pipe( gulp.dest('build/' + dest) );
};

var copy = (pth, pattern, dest) => {
  var the_path = '',
      r        = new RegExp('#', 'g');
  
  if ( typeof pth === 'string' )
    the_path = pth.replace(r, pattern);
  else
    the_path = pth.map( (x) => x.replace(r, pattern) );

  return gulp.src(the_path)
             .pipe( gulp.dest(dest) );
};

// ------------------------------------------------------------------
// Auxiliary tasks

gulp.task( 'clean', (cb) => {
  del([ 'build/*', 'dist/*' ])
    .then( () => { cb(); } );
});

gulp.task( 'build-es6', ['clean'], () => compile(['src/**/*.es6', '!src/**/components/**/*.es6', '!src/js/' + APPLICATION], 'src') );

gulp.task( 'build-react', ['clean'], () => compile(['src/**/components/**/*.es6', 'src/js/' + APPLICATION], 'src') );

gulp.task( 'build-unit-tests', ['clean'], () => compile('test/unit/**/*.es6', 'test/unit') );

gulp.task( 'build-e2e-tests', ['clean'], () => compile('test/e2e/**/*.es6', 'test/e2e') );

gulp.task( 'copy-html', ['clean'], () => copy('src/#/**/*.#', 'html', 'dist') );

gulp.task( 'copy-css', ['clean'], () => copy('src/#/**/*.#', 'css', 'dist') );

gulp.task( 'e2e-tests-start', ['build', 'build-e2e-tests'], () => {
  server.start({
    root: HTTP_ROOT,
    port: HTTP_PORT
  });
  return gulp.src( ['build/test/e2e/**/*.js'], {read: false} )
             .pipe( nightwatch({
               configFile: 'test/e2e/nightwatch.json'
             }) );
});

// ------------------------------------------------------------------
// Main tasks

gulp.task( 'unit-tests', ['build-es6', 'build-unit-tests'], () => {
  return gulp.src( ['build/test/unit/**/*.js'], {read: false} )
             .pipe( mocha({reporter: 'spec'}) );
});

gulp.task( 'build', ['build-es6', 'build-react', 'copy-html', 'copy-css'], () => {
  return gulp.src( 'build/src/js/' + APPLICATION )
      .pipe( webpack({
        output: { filename: APPLICATION }
      }) )
      .pipe( gulp.dest('dist/') );
});

gulp.task( 'e2e-tests', ['e2e-tests-start'], (cb) => {
  server.stop();
  cb();
});

gulp.task( 'test', ['unit-tests', 'e2e-tests'] );

gulp.task( 'dist', ['build-es6', 'build-react', 'copy-html', 'copy-css'], (cb) => {
  pump([
    gulp.src( 'build/src/js/' + APPLICATION ),
    webpack({
      output: { filename: APPLICATION }
    }),
    uglify(),
    gulp.dest('dist/')
  ], cb);
});

